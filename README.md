# Docker - Ionic

![Badge](https://img.shields.io/static/v1?label=ionic&message=8.4.1&color=blue)
![Badge](https://img.shields.io/static/v1?label=node&message=22.12.0&color=blue)
![Badge](https://img.shields.io/static/v1?label=npm&message=10.9.0&color=blue)
![Badge](https://img.shields.io/static/v1?label=cordova&message=12.0.0&color=blue)
![Badge](https://img.shields.io/static/v1?label=capacitor&message=%5E6.2.0&color=blue)
[![Badge](https://img.shields.io/static/v1?label=bitbucket&message=ygo_morais&color=green&logo=bitbucket)](https://bitbucket.org/ygo_morais/docker-ionic)
![Badge](https://img.shields.io/static/v1?label=license&message=MIT&color=green)

## Sobre

Imagem para criação de container para criar aplicativos para dispositivos móveis com Ionic, usando Capacitor ou Cordova.

## Criando um projeto

Por padrão, o projeto é feito em Capacitor, mas para fazer um projeto em Cordova, adicione a opção _--cordova_ no fim do comando abaixo:

```bash
docker container run -ti --rm -v $PWD:/projeto ygoamaral/ionic:latest ionic start <nome-app>
```

_Obs1: depois de executar esse comando, a pasta "\<nome-app>" será automaticamente criada._

_Dica: antes de adicionar as plataformas Android e/ou IOS, defina o nomes do app e pacote de compilação em **capacitor.config.ts**, nas propriedades **appName** e **appId** respectivamente._

## Criando container para o projeto

1 - Habilitando todas as permissão para o projeto:

```bash
chmod 777 -R <nome-app>
```

2 - Acessando o projeto:

```bash
cd <nome-app>
```

3 - Criando o container que será usado para gerenciar instalações de pacotes e geração de instaladores:

```bash
docker container run -ti --name <nome-container> --privileged --net host -v $PWD:/projeto -v /dev/bus/usb/:/dev/bus/usb ygoamaral/ionic:latest bash
```

4 - Execute o comando abaixo, para o projeto ser executado no navegador <http://localhost:8100>:

```bash
ionic serve
```

## Cordova

### Execuntando o app em um dispositivo com atualização automática

1 - Adicione a plataforma alvo (_android_ e/ou _ios_). Esse comando deve ser executado, pelo menos, uma vez no projeto:

```dash
ionic cordova platform add <plataforma>
```

2 - Adicione a coleção de _builders_ para @ionic/angular, caso use o Angular:

```bash
ng add @ionic/cordova-builders
```

3 - Depois de conectar seu dispositivo via cabo USB, encontre o ID do dispositivo:

```bash
adb devices
```

4 - Com esse ID, execute o projeto no dispositivo:

```bash
ionic cordova run android --target=<id-dispositivo> -l
```

_Obs: Em alguns casos, a instalação do app no dispositivo pode demorar cerca de 5 minutos na primeira vez que o comando acima é executado._

## Capacitor

### Execuntando app em um dispositivo com atualização automática

1 - Execute o comando de construção (adicione _--prod_ no final do comando, para construir para ambiente de produção):

```bash
ionic build
```

A pasta _/www_ estará atualizada. Se o passo 2 já foi realizado, copie as mudanças no fonte para os projetos nativos (Android e/ou IOS):

```bash
ionic cap copy
```

_obs: Depois de fazer atualizações na parte nativa do código (como adicionar um novo plugin), use o comando sync_:

```bash
ionic cap sync
```

2 - Adicione a plataforma alvo (_android_ e/ou _ios_). Esse comando deve ser executado, pelo menos, uma vez no projeto:

```bash
ionic cap add <plataforma>
```

3 - Android: Depois de conectar seu dispositivo via cabo USB, encontre o ID do dispositivo:

```bash
adb devices
```

4 - Android: Com o ID do dispositivo, e o ip do seu ambiente de desenvolvimento (ip-host-dev), execute o app diretamente do seu dispositivo:

```bash
ionic cap run android --target=<id-dispositivo> --host=<ip-host-dev> -l
```

_Obs 1: O comando acima, além de executar no dispositivo, também será executado no navegador do host em <http://localhost:8100>._

_Obs 2: A instalação do app no dispositivo pode demorar cerca de 3 minutos na primeira vez que esse comando for executado._

_Obs 3: O dispositivo e o ambiente de desenvolvimento (host) devem está na mesma rede._

_Obs 4: Se no dispositivo aparecer a mensagem: Página da Web não disponível. net::ERR_ADDRESS_UNREACHABLE. Pode ser um bloqueio de firewall de seu ambiente de desenvolvimento. Verifique se a porta 8100, está liberada para acesso externo.._

### Gerando instalador Android

1 - Gerando chave de assinatura. **Atenção! Guarde a localização da chave, seu apelido (alias) e a senha para assinar os apps!** Caso tenha uma chave, ignore esse passo.

```bash
keytool -genkey -v -keystore <nome-chave>.jks -keyalg RSA -keysize 2048 -validity 10000 -alias <alias-chave>
```

2 - Execute os comandos de construção e copia de recursos:

```bash
ionic build --prod && ionic cap copy && ionic cap sync && cd android
```

3 - Gerando arquivo apk:

```bash
./gradlew assembleRelease && cd app/build/outputs/apk/release
```

4 - Compactando o app (dica: use o autocompletar do terminal para visualizar versões do SDK disponíveis):

```bash
/opt/android-sdk-linux/build-tools/<versao-sdk>/zipalign -v -p 4 app-release-unsigned.apk app-unsigned-aligned.apk
```

5 - Assinando o app. Aqui será solicitado a localização, o apelido e a senha da assinatura do passo 1:

```bash
/opt/android-sdk-linux/build-tools/<versao-sdk>/apksigner sign --ks <caminho-chave>.jks --out <nome-arquivo>.apk app-unsigned-aligned.apk
```

6 - Agora instale o <nome\-arquivo>.apk em seu dispositivo.

### Gerando projeto IOS

_Obs: Obrigatório o uso de Mac OS e Xcode._

1 - Execute os comandos:

```bash
ionic build --prod && ionic cap copy && ionic cap sync
```

2 - Abra o arquivo \<projeto>/ios/App/App.xcworkspace com o Xcode para acessar o projeto nativo para IOS.

_Obs: A pasta **/ios** depende da pasta **/node_modules**._

3 - Escolha um emulador e clique em _Run_ para visualizar a aplicação.

## Ícone e Splash Screen

1 - Crie uma pasta na raiz do projeto chamada "resources".

2 - Adicione as imagens "icon.png" e "splash.png" nessa pasta.

2.1 - Dimensões:

- Ícone: 1024×1024px
- Splash screen: 2732×2732px

_Obs: No caso do splash screen, recomenda-se que a imagem fique centralizada em uma área de 1200x1200px._

3 - Execute os comandos para gerar as imagens:

```bash
cordova-res android --skip-config --copy
cordova-res ios --skip-config --copy
```

4 - Logo em seguida, faça a copia e sincronização do projeto (Capacitor):

```bash
ionic cap copy
ionic cap sync
```

## Visualizar Logs no Google Chrome

Quando o projeto estiver sendo executado em um dispositivo em modo de desenvolvimento, é possível visualizar os logs acessando, _chrome://inspect_ (digite na barra de busca/endereço). Localize seu dispositivo e clique em no link: _inspect_.

## Retornando para o projeto

1 - Inicie o container:

```bash
docker container start <nome-container>
```

2 - Acesse o projeto por esse container:

```bash
docker exec -it <nome-container> bash
```

## Falha em requisições http

Poderá ocorrer falhas de requisições http em dispositivos Android 9 ou superior. Adicione a linha abaixo para fazer o ajuste no arquivo _nome-projeto/android/app/src/main/AndroidManifest.xml_:

```xml
  <manifest>
    <application
      ...
      android:usesCleartextTraffic="true" // <-- adicione essa linha
      ...>
      ...
    </application>
  </manifest>
```

_Obs: Atenção! Isso irá remover a restrição: requição somente em https._

## Dicas

### Alterar nome do pacote de compilação

Acesse o arquivo _./capacitor.config.ts_ e altere a propriedade _appId_:

```typescript
const config: CapacitorConfig = {
  appId: 'com.meu-app.app', // <-- altere aqui
  ...
}
```

Caso esteja desenvolvendo para Android, acesse o arquivo _./android/app/build.gradle_ e altere a propriedade _applicationId_:

```groovy
android {
    ...
    defaultConfig {
        applicationId "com.meu-app.app" // <-- altere aqui
        ...
    }
}
```

### Alterando versão do app

Acesse o arquivo _./package.json_ e altere a propriedade _version_:

```json
{
  "name": "meu-app",
  "version": "0.0.1", // <-- altere aqui
  ...
}
```

No Android acesse o arquivo _./android/app/build.gradle_ e altere as propriedades _versionCode_ e _versionName_:

```groovy
android {
    ...
    defaultConfig {
        ...
        versionCode 6 // <-- altere aqui
        versionName "0.0.6" // <-- e aqui
        ...
    }
}
```

No Ios, o arquivo será _./ios/App/App.xcodeproj/project.pbxproj_ e altere os seguintes campos:

- Código da versão: _CURRENT_PROJECT_VERSION_

- Nome da Versão: _MARKETING_VERSION_

- Nome do app (PGP): _INFOPLIST_KEY_CFBundleDisplayName_

Obs: Em alguns casos a propriedade "INFOPLIST_KEY_CFBundleDisplayName" não aparece, bastando somente adiciona-la.
