FROM runmymind/docker-android-sdk:ubuntu-standalone

ENV NODE_VERSION=22.12.0 \
  NPM_VERSION=10.9.2 \
  GRADLE_VERSION=7.1.1 \
  PREFIX=/usr/local \
  COCOAPODS_ALLOW_ROOT=true \
  NPM_CONFIG_PREFIX=/home/node/.npm-global \
  NPM_CONFIG_CACHE=/home/cache \
  PLAYWRIGHT_BROWSERS_PATH=0 \
  CYPRESS_CACHE_FOLDER=/home/cache-cypress

ENV PATH=${PATH}:${ANDROID_HOME}/emulator:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools:/opt/node/bin:${NPM_CONFIG_PREFIX}/bin:/opt/gradle/gradle-"$GRADLE_VERSION"/bin

RUN echo "=== Arquitetura I386 para instalar bibliotecas para o NodeJS ===" \
  && dpkg --add-architecture i386 \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
  libc6:i386 \
  libncurses5:i386 \
  libstdc++6:i386 \
  lib32z1 \
  g++ \
  libssl-dev \
  zlib1g-dev \
  ruby-dev \
  openjdk-17-jdk \
  && echo "=== Gradle ===" \
  && wget https://services.gradle.org/distributions/gradle-"$GRADLE_VERSION"-bin.zip \
  && mkdir /opt/gradle \
  && unzip -d /opt/gradle gradle-"$GRADLE_VERSION"-bin.zip \
  && rm -rf gradle-"$GRADLE_VERSION"-bin.zip \
  && echo "=== NodeJS ===" \
  && mkdir -p ${NPM_CONFIG_PREFIX} \
  && wget -q http://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz \
  && tar -xzf node-v${NODE_VERSION}-linux-x64.tar.gz \
  && mv node-v${NODE_VERSION}-linux-x64 /opt/node \
  && rm node-v${NODE_VERSION}-linux-x64.tar.gz \
  && npm install -g npm@${NPM_VERSION} \
  && echo "=== Ionic ===" \
  && npm i -g cordova @ionic/cli native-run \
  && npm i -g cordova-res --unsafe-perm \
  && npm cache clear --force \
  && gem install sass \
  && echo "=== CocoaPods ===" \
  && gem install cocoapods \
  && pod setup --verbose --allow-root \
  && echo "=== Cypress para ionic com vuejs ===" \
  && mkdir -p /home/cache-cypress \
  && chmod 777 -R /home/cache-cypress/ \
  && echo "=== Apagando pacotes não utilizados ===" \
  && apt-get autoremove -y \
  && apt-get remove -y \
  && apt-get clean

WORKDIR /projeto